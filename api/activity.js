import fly from './fetch.js'

const activityUrl = {
	
	// 发布活动
	activityUpdate: "/activity/update/:activityId/",

	// 活动列表
	activityList: "/activity/list/",
	
	// 活动详情
	activityDetail: "/activity/detail/:activityId/",
	
	// 修改活动状态
	activityStatus: '/activity/:status/:activityId/',
	
	// 删除活动
	activityDelete: '/activity/delete/:activityId/',
	
	// 排期
	scheduleList: '/activity/schedule/list/:userId',
	
	// 排期详情
	scheduleDetail: '/activity/schedule/detail/:userId',
	
	// 报名
	siginUpCreate: '/activity/siginUp/create/:activityId/',
	
	// 取消报名
	siginUpDelete: '/activity/siginUp/delete/:activityId/',
	
	// 报名列表
	siginUpList: '/activity/siginUp/list/:activityId/',
	
	// 创建历史
	createdList: '/activity/record/created/list/',
	
	// 参与历史
	takePartIn: '/activity/record/takePartIn/list/'
}

export function activityUpdate(activityId, data){
	let url = activityUrl.activityUpdate.replace(':activityId', activityId)
	return fly.post(url, data)
}

export function activityList(params){
	return fly.get(activityUrl.activityList, params)
}

export function activityDetail(activityId){
	let url = activityUrl.activityDetail.replace(':activityId', activityId)
	return fly.get(url)
}

export function activityStatus(status, activityId){
	let url = activityUrl.activityStatus.replace(':activityId', activityId).replace(':status', status)
	return fly.post(url)
}

export function activityDelete(activityId){
	let url = activityUrl.activityDelete.replace(':activityId', activityId)
	return fly.delete(url)
}

export function scheduleList(userId, params){
	let url = activityUrl.scheduleList.replace(':userId', userId)
	return fly.get(url, params)
}

export function scheduleDetail(userId, params){
	let url = activityUrl.scheduleDetail.replace(':userId', userId)
	return fly.get(url, params)
}

export function siginUpCreate(activityId){
	let url = activityUrl.siginUpCreate.replace(':activityId', activityId)
	return fly.post(url)
}

export function siginUpDelete(activityId){
	let url = activityUrl.siginUpDelete.replace(':activityId', activityId)
	return fly.delete(url)
}

export function siginUpList(activityId){
	let url = activityUrl.siginUpList.replace(':activityId', activityId)
	return fly.get(url)
}

export function createdList(params){
	let url = activityUrl.createdList
	return fly.get(url, params)
}

export function takePartIn(params){
	let url = activityUrl.takePartIn
	return fly.get(url, params)
}