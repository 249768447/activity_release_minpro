import fly from './fetch.js'

const settingUrl = {
	helpContent: "/setting/help/",
	serverContent: "/setting/server/",
	aboutContent: "setting/about/"
}

export function helpContent(){
	return fly.get(settingUrl.helpContent)
}

export function serverContent(){
	return fly.get(settingUrl.serverContent)
}

export function aboutContent(){
	return fly.get(settingUrl.aboutContent)
}