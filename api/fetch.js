import {BASE_URL} from '../config/index.js'

let Fly = require('flyio/dist/npm/wx')
let fly = new Fly

//设置超时
fly.config.timeout = 30000;
//设置请求基地址
console.log(BASE_URL)
fly.config.baseURL = BASE_URL

// 添加请求拦截器
fly.interceptors.request.use((request) => {
	
	console.log('---------- 请求拦截 ----------')
	console.log(request)
	
	
	
    let token = uni.getStorageSync('token')
    request.headers['Authorization'] = token || '';
	
    return request
	
}, function(error) {
    return Promise.reject(error)
})

// 添加响应拦截器
fly.interceptors.response.use((response) => {
	
    console.log('---------- 响应拦截 ----------')
	console.log(response)
	
	// 非第三方请求,且状态码为9,则要求授权登录
	if(response.request.baseURL === BASE_URL && response.data.rc === 9){
		reqAuth()
	}
    return response
	
}, (error) => {
    return Promise.reject(error)
})

export default fly