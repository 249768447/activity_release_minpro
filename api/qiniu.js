import fly from './fetch.js'

const qiniuUrl = {
	qiniuToken: "/user/api/qiniu/token/",
}

export function qiniuToken(){
	return fly.get(qiniuUrl.qiniuToken)
}
