import fly from './fetch.js'

const userUrl = {

	// 登录
	login: "/user/login/",
	
	// 用户详情
	detail: "/user/detail/",
	
	// 更新用户信息
	update: '/user/update/',
	
	// 关注\取消关注
	focusOn: '/user/focusOn/:status/:userId/',
	
	// 关注列表
	focusOnList: '/user/focusOn/list/',
	
	// 账号充值
	recharge: 'user/recharge/',
	
	// 更新二维码
	updateQrCode: 'user/setting/qrcode/',
	
	// 财富值
	money: 'user/money/'
}

export function login(data){
	return fly.post(userUrl.login, data)
}

export function detail(params){
	return fly.get(userUrl.detail, params)
}

export function update(data){
	return fly.post(userUrl.update, data)
}

export function focusOn(status, userId){
	let url = userUrl.focusOn.replace(':status', status).replace(':userId', userId)
	return fly.get(url)
}

export function focusOnList(params){
	return fly.get(userUrl.focusOnList, params)
}

export function recharge(data){
	return fly.post(userUrl.recharge, data);
}

export function updateQrCode(data){
	return fly.post(userUrl.updateQrCode, data);
}

export function money(params){
	return fly.get(userUrl.money, params)
}