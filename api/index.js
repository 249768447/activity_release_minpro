import * as userApi from './user'
import * as tencentApi from './tencent'
import * as qiniuApi from './qiniu'
import * as activityApi from './activity'
import * as settingApi from './setting.js'

const api = {
	userApi,
	tencentApi,
	qiniuApi,
	activityApi,
	settingApi
}

export default api