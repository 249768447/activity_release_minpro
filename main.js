import Vue from 'vue'
import Vuex from 'vuex'
import boboMessage from '@/components/bobo-message/bobo-message.vue'
import App from './App'
import api from './api'

Vue.config.productionTip = false
Vue.prototype.$api = api
Vue.component('message', boboMessage)


App.mpType = 'app'

const app = new Vue({
	...App,
	created() {
		wx.cloud.init();
		// 静默登录,没有用户信息
		let that = this
		wx.login({
			success(res){
				let data = {
					code: res.code
				}
				let token = uni.getStorageSync('token')
				
				// 每次都刷新用户信息
				that.$api.userApi.login(data).then(resp =>{
					if(resp.data.rc !== 0){
						uni.showToast({
							title:'登录失败,请退出重试!'
						})
					}else{
						uni.setStorageSync('token', resp.data.data.token)
						uni.setStorageSync('userId', resp.data.data.userId),
						uni.setStorageSync('qrCodeUrl', resp.data.data.qr_code)
					}
				})
			},
			fail(err) {
				console.log(err)
			}
		})
	}
})
app.$mount()
