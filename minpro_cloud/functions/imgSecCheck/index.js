// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
	try {
		let value = event.value;
		// 下载图片
		const res = await cloud.openapi.security.imgSecCheck({
			media: {
				contentType: 'image/png',
				value: Buffer.from(value)
			}
		})
		return res;

	} catch (err) {
		return err;
	}
}
